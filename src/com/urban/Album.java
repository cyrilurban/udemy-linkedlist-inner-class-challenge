package com.urban;

import java.util.ArrayList;

public class Album {
    private String name;
    private SongList album;

    public Album(String name) {
        this.name = name;
        this.album = new SongList();
    }

    public String getName() {
        return name;
    }

    public Song getSong(int index) {
        return this.album.getSong(index);
    }

    public int getLength() {
        return this.album.getLength();
    }

    public void addSong(Song song) {
        this.album.addSong(song);
    }

    public void printSongs() {
        this.album.printSongs();
    }
    
    private class SongList {
        private ArrayList<Song> songs;

        public SongList() {
            this.songs = new ArrayList<Song>();
        }

        public Song getSong(int index) {
            try {
                return songs.get(index);
            } catch (IndexOutOfBoundsException e) {
                return null;
            }
        }

        public int getLength() {
            return this.songs.size();
        }

        public void addSong(Song song) {
            if(!alreadyExists(song)) {
                this.songs.add(song);
            } else {
                System.out.println("The song is already in the songs.");
            }
        }

        public void printSongs() {
            System.out.println("Album " + name + ":");
            int timeSec, min, sec;

            for (int i = 0; i < this.songs.size(); i++) {
                timeSec = this.songs.get(i).getDuration();
                min = timeSec / 60;
                sec = timeSec % 60;

                System.out.println(min + ":" + sec + " - " + this.songs.get(i).getTitle());
            }
        }

        private boolean alreadyExists(Song song) {
            for (int i = 0; i < this.songs.size(); i++) {
                if (this.songs.get(i).getTitle() == song.getTitle()) {
                    return true;
                }
            }
            return false;
        }
    }
}
