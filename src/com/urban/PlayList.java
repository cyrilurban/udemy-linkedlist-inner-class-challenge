package com.urban;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

public class PlayList {
    private String name;
    private LinkedList<Song> playlist;
    private boolean goingForward;
    private ListIterator<Song> songs;

    public PlayList(String name) {
        this.name = name;
        this.playlist = new LinkedList<Song>();
        this.goingForward = true;
    }

    // position index from 0
    public void addSong(Album album, int position) {
        if (exists(album, position) && !isInPlaylist(album, position)) {
            Song song = album.getSong(position);
            playlist.add(song);
            songs = this.playlist.listIterator();
        }
    }

    public void replaySong() {
        if(!goingForward) {
            if (this.songs.hasNext()) {
                this.songs.next();
                System.out.println("REPLAY. Playing song: " + this.songs.previous().getTitle());
            } else {
                System.out.println("Nothing to REPLAY");
            }
        } else {
            if (this.songs.hasPrevious()) {
                this.songs.previous();
                System.out.println("REPLAY. Playing song: " + this.songs.next().getTitle());
            } else {
                System.out.println("Nothing to REPLAY");
            }
        }
    }

    public void skipForwardSong() {
        if(!goingForward) {
            if(this.songs.hasNext()) {
                this.songs.next();
            }
            goingForward = true;
        }

        if(this.songs.hasNext()) {
            System.out.println("NEXT. Playing song: " + this.songs.next().getTitle());
        } else {
            System.out.println("Reached the end of the playlist");
            goingForward = false;
        }
    }

    public void skipBackwardSong() {
        if(goingForward) {
            if(this.songs.hasPrevious()) {
                this.songs.previous();
            }
            goingForward = false;
        }

        if(this.songs.hasPrevious()) {
            System.out.println("BACK. Playing song: " + this.songs.previous().getTitle());
        } else {
            System.out.println("Reached the start of the playlist");
            goingForward = true;
        }
    }

    private boolean isInPlaylist(Album album, int position) {
        ListIterator<Song> songs = this.playlist.listIterator();
        Song newSong = album.getSong(position);

        while(songs.hasNext()) {
            if(songs.next().equals(newSong)) {
                System.out.println("The song is already in the playlist.");
                return true;
            }
        }

        return false;
    }

    private boolean exists(Album album, int position) {
        if (album.getLength() > position) {
            return true;
        }

        System.out.println("Any song is not on this position in this album.");
        return false;
    }
}
