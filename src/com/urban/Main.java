package com.urban;

public class Main {

    public static void main(String[] args) {
        Album aviciiMix = new Album("Avicii Mix");
        aviciiMix.addSong(new Song("Wake me up", 143));
        aviciiMix.addSong(new Song("Levels", 161));
        aviciiMix.addSong(new Song("Hey brother", 152));
        aviciiMix.addSong(new Song("Waiting for love", 129));

        Album diabloMix = new Album("Don Diablo Mix");
        diabloMix.addSong(new Song("Switch", 193));
        diabloMix.addSong(new Song("Children of the miracle", 169));
        diabloMix.addSong(new Song("Dancing", 173));

        PlayList playList = new PlayList("Cyril Mix");
        playList.addSong(aviciiMix, 0); // Wake me up
        playList.addSong(diabloMix, 2); // Dancing
        playList.addSong(diabloMix, 0); // Switch

        // simulation of actions instead of dynamic keyboard control...
        playList.replaySong();
        playList.skipForwardSong();
        playList.replaySong();
        playList.skipForwardSong();
        playList.replaySong();
        playList.skipForwardSong();
        playList.replaySong();
        playList.skipBackwardSong();
        playList.replaySong();
        playList.skipBackwardSong();
        playList.replaySong();
        playList.skipBackwardSong();
        playList.replaySong();
    }
}
